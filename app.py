import sys, os, gunicorn

sys.path.insert(0, 'controllers')

from config import *

import user, event, registration, coupon, tag 
#mport *

@app.route('/')
def home():
	return render_template("index.html")

if __name__ == '__main__':
    init_db()
    app.secret_key = os.urandom(512)
    app.run(debug=True, host='0.0.0.0')