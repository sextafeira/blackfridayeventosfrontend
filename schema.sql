-- DROP STATEMENTS
DROP TABLE IF EXISTS event;
DROP TABLE IF EXISTS user;

-- CREATE STATEMENTS
CREATE TABLE IF NOT EXISTS event(
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  name TEXT,
  url TEXT,
  date TEXT
);

CREATE TABLE IF NOT EXISTS user(
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  username TEXT,
  password TEXT
);

CREATE TABLE IF NOT EXISTS tag(
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  name TEXT
);

CREATE TABLE IF NOT EXISTS tag_user(
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  tag_id INTEGER,
  user_id INTEGER
);

-- CREATE STATEMENTS
INSERT INTO event(name,url,date) VALUES ("A volta dos que nao foram", "https://i.ytimg.com/vi/GqrHBWTbeWY/hqdefault.jpg", "2015-01-01");
INSERT INTO event(name,url,date) VALUES ("Lula Molusca da Silva Sauro", "http://www.culturamix.com/wp-content/gallery/lula-molusco-2/275737_Papel-de-Parede-Patrick-Lula-Molusco-e-Bob-Esponja_1920x1200.jpg", "2015-01-01");
INSERT INTO event(name,url,date) VALUES ("Encontro das Retas paralelas", "http://www.humornaciencia.com.br/matematica/retas.jpg", "2015-01-01");
INSERT INTO event(name,url,date) VALUES ("Feijoada Vegetariana", "http://1.bp.blogspot.com/-ArX-dSPKIQM/UfE3bRL9wEI/AAAAAAAAVN0/kKTyuRmcBRk/s1600/feijoada-de-legumes.jpg", "2015-01-01");

INSERT INTO user(username, password) VALUES ("daniel", "daniel");
INSERT INTO user(username, password) VALUES ("mateus", "mateus");
INSERT INTO user(username, password) VALUES ("aleksander", "aleksander");
INSERT INTO user(username, password) VALUES ("wildrimak", "wildrimak");

INSERT INTO tag(name) VALUES ("banco de dados");
INSERT INTO tag(name) VALUES ("fisica");

INSERT INTO tag_user(tag_id, user_id) VALUES (1,1);
INSERT INTO tag_user(tag_id, user_id) VALUES (1,2);
INSERT INTO tag_user(tag_id, user_id) VALUES (2,1);
INSERT INTO tag_user(tag_id, user_id) VALUES (3,1);
INSERT INTO tag_user(tag_id, user_id) VALUES (4,2);

-- ADDITIONAL TOOLS:
