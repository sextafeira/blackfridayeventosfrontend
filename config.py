import requests
import sqlite3
from flask import Flask, flash, session, redirect, url_for, escape, request, render_template, g, abort
from flask_wtf import *
from flask_qrcode import QRcode     
from functools import wraps
import json

app = Flask(__name__)
qrcode = QRcode(app)
DATABASE = 'database.db'
URL = 'https://blackfridayeventosapi.herokuapp.com/'
#URL = 'http://localhost:8080/'
HEADERS = {'content-type': 'application/json'}

def get_db():
    db = getattr(g, '_database', None)
    if db is None:
        db = g._database = sqlite3.connect(DATABASE)
        db.row_factory = make_dicts
    return db

def login_required(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        user = getattr(g, '_user', None)
        print(user)
        if user is None:
            return redirect(url_for('login'))
        return f(*args, **kwargs)
    return decorated_function

def init_db():
    with app.app_context():
        db = get_db()
        with app.open_resource('schema.sql', mode='r') as f:
            db.cursor().executescript(f.read())
        db.commit()

def make_dicts(cursor, row):
    return dict((cursor.description[idx][0], value)
                for idx, value in enumerate(row))

def query_db(query, args=(), one=False):
    cur = get_db().execute(query, args)
    rv = cur.fetchall()
    cur.close()
    return (rv[0] if rv else None) if one else rv

@app.teardown_appcontext
def close_connection(exception):
    db = getattr(g, '_database', None)
    if db is not None:
        db.close()