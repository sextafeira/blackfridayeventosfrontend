# Friday Team

## BlackFridayEventosFrontend

Web application that consumes an API of Events Application

### Prototype (V1):
	
	Web platform which can handle event data in order to improve the user experience with web platform for events.

## Project Artifacts

## Members:

* [Alexsander Magnum](https://github.com/10Alexsander10)
* [Daniel Farias](https://github.com/danieldsf)
* [Mateus Oliveira](https://github.com/M4T3U5)
* [Wildrimak Pereira](https://github.com/Wildrimak)

### Technologies:

* [Flask](http://flask.pocoo.org/)
* [NPM](https://www.npmjs.com/)
* [Jquery](https://jquery.com/)
* [Angular](https://angularjs.org/)
* [Bootstrap (Yeti)](https://bootswatch.com/yeti/)

### Another cool stuffs

[Project Planning](https://trello.com/b/2w91uXMv)

[Deploy URL](https://blackfridayeventos.herokuapp.com/)


## Cool comand:

pipreqs BlackFridayEventosFrontend --ignore BlackFridayEventosFrontend\static --force
	
