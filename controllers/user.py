from config import *

@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        r = requests.post(URL+'users/login', data=json.dumps(request.form), headers=HEADERS)
        if r.status_code == 200:
            session['user'] = g._user = r.json() 
            print(g._user)
            session['logged_in'] = True
            flash(u'You were logged in', 'success')   
            return redirect(url_for('index_event'))
        else:
            flash(u'Wrong e-mail or password!', 'error')
    return render_template('user/login.html')

@app.route("/logout", methods=['GET'])
def logout():
    session['logged_in'] = False
    session['user'] = {}
    flash(u'You were logged out', 'success')
    return redirect(url_for('login'))

@app.route('/sign_up', methods=['GET', 'POST'])
def sign_up():
    if request.method == 'POST':
        r = requests.post(URL+'users', data=json.dumps(request.form), headers=HEADERS)
        if r.status_code == 201:
            flash(u'User created successfully', 'success')
            return render_template('user/login.html', req=r)
        else:
            flash(u'Not possible to create a user!', 'error')
    else:
        return render_template('user/signup.html')