from flask_wtf import Form
from wtforms import * 
from wtforms.validators import *

class LoginForm(Form):
	email = StringField('email', validators=[InputRequired()])
	password = PasswordField('password', validators=[InputRequired()])

